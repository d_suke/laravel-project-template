# Laravel-Project-Template
laravel project template

# Getting started

## Installation

Clone the repository

    PS> git clone https://d_suke@bitbucket.org/d_suke/laravel-project-template.git

Rename and Switch to the repo folder

    PS> Rename-Item Laravel-Project-Template -newname project-name

    PS> cd project-name

start container use docker-compose

    PS> docker-compose up

Open new terminal. install laravel library

    PS> docker-compose exec app composer install
    
or

create new laravel project

    PS> docker-compose exec app bash
    root@container:/var/www# laravel new

## create

controller/model create

    PS> docker-compose exec app php artisan make:controller Api/HelloController -r
    if create migrate file add 'm' option 
    PS> docker-compose exec app php artisan make:model Hello -m
    else
    PS> docker-compose exec app php artisan make:model Hello
    
migrate

    PS> docker-compose exec app php artisan migrate
    PS> docker-compose exec app php artisan migrate:refresh
    
add seeder

    PS> docker-compose exec app composer dump-autoload
    PS> docker-compose exec app php artisan db:seed --class=HelloTableSeeder

url

    http://localhost/api/hello

## tips. docker commands

[container一覧]

    PS> docker-compose ps

[container削除]

    PS> docker rm $(docker ps -q -a)

[未使用イメージ/コンテナ/キャッシュ削除]

    PS> docker system prune 

[configキャッシュ削除]

    PS> docker-compose exec app php artisan config:cache 
    
